CREATE TABLE `stdAccount` (
`stdAcc_ID` char(12) NOT NULL,
`stdAcc_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`stdAcc_Lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`stdAcc_Nickname` varchar(120) COLLATE utf8_unicode_ci DEFAULT NULL,
`stdAcc_Blood` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
`stdAcc_Gender` char(1) NOT NULL DEFAULT '0',
`stdAcc_Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`stdAcc_Tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
`stdAcc_Faculty` int(2) unsigned NOT NULL,
`stdAcc_Major` int(2) unsigned NOT NULL,
`stdAcc_Year` varchar(1) COLLATE utf8_unicode_ci DEFAULT '1',
PRIMARY KEY (`stdAcc_ID`),
UNIQUE KEY `stdAcc_ID` (`stdAcc_ID`),
CONSTRAINT `stdAcc_Major_fk` FOREIGN KEY (`stdAcc_Major`) REFERENCES `major` (`major_ID`) ON UPDATE CASCADE,
CONSTRAINT `stdAcc_Faculty_fk` FOREIGN KEY (`stdAcc_Faculty`) REFERENCES `faculty` (`faculty_ID`) ON UPDATE CASCADE 
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `staffAccount` (
`staffAcc_ID` varchar(12) NOT NULL,
`staffAcc_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`staffAcc_Lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`staffAcc_gender` char(1) NOT NULL DEFAULT '0',
`staffAcc_Email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
`staffAcc_Tel` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
`staffAcc_Faculty` int(2) unsigned NOT NULL,
PRIMARY KEY (`staffAcc_ID`),
UNIQUE KEY `staffAcc_ID` (`staffAcc_ID`),
CONSTRAINT `staffAcc_Faculty_fk` FOREIGN KEY (`staffAcc_Faculty`) REFERENCES `faculty` (`faculty_ID`) ON UPDATE CASCADE 
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `faculty` (
`faculty_ID` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
`faculty_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'N/A',
PRIMARY KEY (`faculty_ID`),
UNIQUE KEY `faculty_ID` (`faculty_ID`)
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `major` (
`major_ID` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
`major_Faculty` int(2) unsigned NOT NULL,
`major_Name` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'N/A',
PRIMARY KEY (`major_ID`),
UNIQUE KEY `major_ID` (`major_ID`),
CONSTRAINT `major_Faculty_fk` FOREIGN KEY (`major_Faculty`) REFERENCES `faculty` (`faculty_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `eventType` (
`eventType_ID` int(2) unsigned zerofill NOT NULL AUTO_INCREMENT,
`eventType_Name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
`eventType_TimeUnit` int(3) unsigned DEFAULT '0',
PRIMARY KEY (`eventType_ID`),
UNIQUE KEY `eventType_ID` (`eventType_ID`)
)ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `event` (
`event_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
`event_Name` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
`event_Detail` text COLLATE utf8_unicode_ci DEFAULT NULL,
`event_Type` int(2) unsigned NOT NULL,
`event_Sdate` date NOT NULL,
`event_Fdate` date NOT NULL,
`event_Stime` time NOT NULL,
`event_Ftime` time NOT NULL,
`event_Faculty` int(2) unsigned NOT NULL,
`event_Target` varchar(8) NOT NULL,
`event_Staff` varchar(12) NOT NULL,
PRIMARY KEY (`event_ID`),
UNIQUE KEY `event_ID` (`event_ID`),
CONSTRAINT `event_Type_fk` FOREIGN KEY (`event_Type`) REFERENCES `eventType` (`eventType_ID`) ON UPDATE CASCADE,
CONSTRAINT `event_Faculty_fk` FOREIGN KEY (`event_Faculty`) REFERENCES `faculty` (`faculty_ID`) ON UPDATE CASCADE,
CONSTRAINT `event_Staff_fk` FOREIGN KEY (`event_Staff`) REFERENCES `staffAccount` (`staffAcc_ID`) ON UPDATE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE `joinEvent` (
`join_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
`join_EventID` int(11) unsigned NOT NULL,
`join_stdAccID` char(12) NOT NULL,
`join_Timestamp` datetime NOT NULL,
PRIMARY KEY (`join_ID`),
UNIQUE KEY join_ID (`join_ID`),
CONSTRAINT `join_EventID_fk` FOREIGN KEY (`join_EventID`) REFERENCES `event` (`event_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT `join_stdAccID_fk` FOREIGN KEY (`join_stdAccID`) REFERENCES `stdAccount` (`stdAcc_ID`) ON DELETE CASCADE ON UPDATE CASCADE
)ENGINE=INNODB DEFAULT CHARSET=utf8 COLLATE utf8_unicode_ci;