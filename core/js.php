<?php 

    include 'core.php';
    $cmd = new command();
    $tmp_name = ['one', 'two', 'three'];
    $data;

    // -- main if
    if(isset($_GET['action'])){

        // echo $_GET['action'];

        # example
        if($_GET['action'] == 'ex'){
            foreach ($tmp_name as $key => $value) {
                $tmp[] = [
                    "name" => $value,
                    "No" => $key + 1
                ];
            }
            echo json_encode($tmp);
        
        }else if($_GET['action'] == 'showCriterionDetail'){

            # Description

            $data = $cmd->sql("SELECT `CdName` FROM `CriterionDescriptions` WHERE `CdCriterion` = '7'");
            foreach ($data as $key => $value){
                $tmp[] = [
                    "No" => $key,
                    "CdName" => $value[CdName]
                ];
            }
            echo json_encode($tmp);

        # Critetion
        }else if($_GET['action'] == 'showCriterion'){
            $data = $cmd->sql("");
  
        } else if ($_GET['action'] == 'showChecklist') {

            #Checklist data 

            $data = $cmd->sql("SELECT `CclPoint`,`CclName` FROM `criterionchecklists` WHERE `CclCriterion` = '7'");

            foreach ($data as $key => $value) {
            $sql = "SELECT f.`FileName`, f.`FileDirectory`, f.`FileCclCriterion`, f.`FileCriterionID`, f.`FileVersion`
                    FROM `file` f
                    INNER JOIN(
                        SELECT `FileName`,`FileCclCriterion`,`FileCriterionID`, MAX(`FileVersion`) AS FileVersion
                        FROM `file`
                        WHERE `FileCriterionID` = '7' AND `FileCclCriterion` = '".($key+1)."'           
                        GROUP BY `FileCriterionID`, `FileCclCriterion`, `FileName`
                    ) g
                    ON f.`FileVersion` = g.FileVersion
                    AND f.`FileName` = g.`FileName`
                    AND f.`FileCclCriterion` = g.`FileCclCriterion`
                    AND f.`FileCriterionID` = g.`FileCriterionID`;";
                    
                $detail = $cmd->sql("SELECT `WorkDetials`, `WorkScore` FROM `work` WHERE `WorkCriterionID` = '7' AND `WorkCclCriterion`= '".($key+1)."'");
                $files = $cmd->sql($sql);
                // print_r($detail);
                foreach ($files as $keyfile => $valuefiles){
                    $file[] = [
                        "No" => $keyfile,
                        "FileName" => $valuefiles[FileName],
                        "FileDirectory" => $valuefiles[FileDirectory],
                        "FileVersion" => $valuefiles[FileVersion]
                    ];
                }

                if($detail[0][WorkScore] == 0 || $detail[0][WorkScore] == null){
                    $score = "N/A";
                }else{
                    $score = $detail[0][WorkScore];
                }
                $tmp[] = [
                    "No" => $key,
                    "CdName" => $value[CclName],
                    "Score" => $score,
                    "Details" => $detail[0][WorkDetials],
                    "File" => $file

                ];
                $file = null;
            }
            echo json_encode($tmp);
        
        } else if ($_GET['action'] == 'formChecklist') {

            # form point

            $list = $cmd->sql("SELECT `CclPoint`,`CclName` FROM `criterionchecklists` WHERE `CclCriterion` = '7' AND `CclPoint`='" . $_GET[point] . "'");

            $sql = "SELECT f.`FileName`, f.`FileDirectory`, f.`FileCclCriterion`, f.`FileCriterionID`, f.`FileVersion`
                    FROM `file` f
                    INNER JOIN(
                        SELECT `FileName`,`FileCclCriterion`,`FileCriterionID`, MAX(`FileVersion`) AS FileVersion
                        FROM `file`
                        WHERE `FileCriterionID` = '7' AND `FileCclCriterion` = '" . $_GET[point] . "'           
                        GROUP BY `FileCriterionID`, `FileCclCriterion`, `FileName`
                    ) g
                    ON f.`FileVersion` = g.FileVersion
                    AND f.`FileName` = g.`FileName`
                    AND f.`FileCclCriterion` = g.`FileCclCriterion`
                    AND f.`FileCriterionID` = g.`FileCriterionID`;";

            $files = $cmd->sql($sql);
            foreach ($files as $keyfile => $valuefile) {
                $file[] = [
                    "No" => $keyfile,
                    "FileName" => $valuefile[FileName],
                    "FileDirectory" => $valuefile[FileDirectory],
                    "FileVersion" => $valuefile[FileVersion]
                ];
            }

            $detail = $cmd->sql("SELECT `WorkDetials`, `WorkScore` FROM `work` WHERE `WorkCriterionID` = '7' AND `WorkCclCriterion`= '" . $_GET[point] . "'");
            if ($detail[0][WorkScore] == 0 || $detail[0][WorkScore] == null) {
                $score = "N/A";
            } else {
                $score = $detail[0][WorkScore];
            }
            $sources = $cmd->sql("SELECT `CsName` FROM `criterionsources` WHERE `CsCriterion` = '7'");
            foreach ($sources as $keysource => $valuesource){
                $source[] = [
                    "No" => $keysource,
                    "Src" => $valuesource[CsName]
                ];
            }
            $tmp[] = [
                "No" => $list[0][CclPoint],
                "Description" => $list[0][CclName],
                "Score" => $score,
                "Details" => $detail[0][WorkDetials],
                "File" => $file,
                "Src" => $source

            ];

            echo json_encode($tmp);
       
        }else if($_GET['action'] == 'fileslist'){

            # file list all Criterion

            $sql = "SELECT f.`FileName`, f.`FileDirectory`, f.`FileCclCriterion`, f.`FileCriterionID`, f.`FileVersion`, f.`FileOwn`
                    FROM `file` f
                    INNER JOIN(
                        SELECT `FileName`,`FileCclCriterion`,`FileCriterionID`, MAX(`FileVersion`) AS FileVersion
                        FROM `file`
                        WHERE `FileCriterionID` = '7'           
                        GROUP BY `FileCriterionID`, `FileCclCriterion`, `FileName`
                    ) g
                    ON f.`FileVersion` = g.FileVersion
                    AND f.`FileName` = g.`FileName`
                    AND f.`FileCclCriterion` = g.`FileCclCriterion`
                    AND f.`FileCriterionID` = g.`FileCriterionID`;";

            $files = $cmd->sql($sql);
            foreach ($files as $keyfile => $valuefile) {
                $owner = $cmd->sql("SELECT `AccName`, `AccLastname` FROM `account` WHERE `AccID` = '" . $valuefile[FileOwn] . "'");
                $file[] = [
                    "No" => $keyfile,
                    "FileName" => $valuefile[FileName],
                    "FileDirectory" => $valuefile[FileDirectory],
                    "FileVersion" => $valuefile[FileVersion],
                    "FileOwner" => $owner[0][AccName] . " " . $owner[0][AccLastname]
                ];
        }
            echo json_encode($file);
            
        }else{

            # -- end in action true

            echo json_encode("Error");
        }
            
        
        
    //--end main true  
    }else{

    //--end main false  
    }
    //-- end main if  
    
?>