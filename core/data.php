
    <?php
    session_start();
    require 'core.php';
    $cmd = new command();
      
      // -----------------
      // -- Authentication Process
      // -----------------

      // -- Session
    if (isset($_GET['session'])) {
        if ($_SESSION['loggedin'] == true) { //QUERY AND RETURN DATA 
            $tmp_data = $cmd->db->query("SELECT * FROM `account` WHERE `AccEmail` = '$_GET[username]' ")->fetch(PDO::FETCH_ASSOC);
            $user_data = [
                'AccID' => $tmp_data['AccID'],
                'AccName' => $tmp_data['AccName'],
                'AccLastname' => $tmp_data['AccLastname'],
                'AccEmail' => $tmp_data['AccEmail'],
                'AccTelephone' => $tmp_data['AccTelephone'],
                'AccStatus' => $tmp_data['AccStatus']
            ];
            echo json_encode(['session' => $_SESSION['loggedin'], 'user_data' => $user_data]);
        } else {
            $user_data = [ 
                'AccID' => null,
                'AccName' => null,
                'AccLastname' => null,
                'AccEmail' => null,
                'AccTelephone' => null,
                'AccStatus' => null
            ];
            echo json_encode(['session' => $_SESSION['loggedin'], 'user_data' => $user_data]);
        }
    }

      // -- login 
    else if (isset($_POST['login'])) { //QUERY AND CHECK DATA
        $status_check = false;
        $username_check = $cmd->db->query("SELECT `AccEmail` FROM `account` WHERE `AccEmail` = '$_POST[username]' ");

        if ($username_check->rowCount() == 1) {
            $username_check = $username_check->fetch(PDO::FETCH_ASSOC);
            $username_check = $username_check['AccEmail'];

            $password_check = $cmd->db->query("SELECT `AccID`,`AccPassword` FROM `account` WHERE `AccEmail` = '$username_check' ");
            if ($password_check->rowCount() == 1) {
                $password_check = $password_check->fetch(PDO::FETCH_ASSOC);
                if ($password_check['AccPassword'] != null) {
                    $password_check = $password_check['AccPassword'];
                } else {
                    $password_check = $password_check['AccID'];
                }
                if (password_verify($_POST['password'], $password_check) || $_POST['password'] == $password_check) {
                    $status_check = true;

                } else {
                    $status_check = false;

                }
            } else {
                $status_check = false;
            }
        } else {
            $status_check = false;
        }
        // $test = ['testCode'=> $password_check];
        // echo json_encode(['user_data'=>$test]);
        if ($status_check) {
            $_SESSION['loggedin'] = true;
            $tmp_data = $cmd->db->query("SELECT * FROM `account` WHERE `AccEmail` = '$username_check' ")->fetch(PDO::FETCH_ASSOC);
            $user_data = [
                'AccID' => $tmp_data['AccID'],
                'AccName' => $tmp_data['AccName'],
                'AccLastname' => $tmp_data['AccLastname'],
                'AccEmail' => $tmp_data['AccEmail'],
                'AccTelephone' => $tmp_data['AccTelephone'],
                'AccStatus' => $tmp_data['AccStatus']
            ];
            echo json_encode(['session' => $_SESSION['loggedin'], 'user_data' => $user_data]);
        } else {
            $_SESSION['loggedin'] = false;
            $user_data = [
                'AccID' => null,
                'AccName' => null,
                'AccLastname' => null,
                'AccEmail' => null,
                'AccTelephone' => null,
                'AccStatus' => null
            ];
            echo json_encode(['session' => $_SESSION['loggedin'], 'user_data' => $user_data]);
        }
    }

      // -- logout
    else if (isset($_GET['logout'])) { //DESTROY SESSION HERE
        $_SESSION['loggedin'] = false;
        $user_data = [
            'AccID' => null,
            'AccName' => null,
            'AccLastname' => null,
            'AccEmail' => null,
            'AccTelephone' => null,
            'AccStatus' => null
        ];
        echo json_encode(['session' => $_SESSION['loggedin'], 'user_data' => $user_data]);
    }

      // -----------------
      // -- Form Process [POST Method]
      // -----------------

      // -- add account form
    else if (isset($_POST['adduser'])) {

        $cmd->db->query("INSERT INTO `account` (`AccID`, `AccName`, `AccLastname`, `AccEmail`, `AccTelephone`) Value ('$_POST[AccID]', '$_POST[AccName]', '$_POST[AccLastname]','$_POST[AccEmail]', '$_POST[AccTelephone]')");
        $json_from = [
            'status' => 'successful'
        ];
        echo json_encode($json_from);
    }

      // -- delete account
    else if (isset($_POST['deluser'])) {
        $cmd->db->query("DELETE FROM `account` WHERE `AccID` like '$_POST[AccID]'");
        $json_from = [
            'status' => 'successful'
        ];
        echo json_encode($json_from);
    }

      // -- edit account
    else if (isset($_POST['edituser'])) {
        $cmd->db->query("UPDATE `account` SET `AccStatus`='$_POST[accstatus]' WHERE `AccID` like '$_POST[AccID]'");
        $json_from = [
            'status' => 'successful'
        ];
        echo json_encode($json_from);
        // ADM USR
    }

      //-- add work 
    else if (isset($_POST['savelist'])) {
        $state = 'n/a';
        $docname = $_FILES['files']['name'];
        $directory = "files/" . $_FILES['files']['name'];
        $docversion = $cmd->db->query("SELECT `FileName` FROM `file` WHERE `FileName` = '$docname' AND `FileCriterionID` = '$_POST[WorkCriterionID]' AND `FileCclCriterion` = '$_POST[WorkCclCriterion]'")->rowCount();
        $docversion += 1;
        $date_now = date("Y-m-d H:i:s");
        $cmd->db->query("INSERT INTO `file`(`FileCriterionID`, `FileCclCriterion`, `FileName`, `FileDirectory`, `FileVersion`, `FileDateUpload`, `FileOwn`) VALUES ('$_POST[WorkCriterionID]', '$_POST[WorkCclCriterion]', '$docname', '$directory', '$docversion', '$date_now', '553120290135')");

        if (move_uploaded_file($_FILES['files']['tmp_name'], $directory)) {
            $state = "Copy/Upload Complete";
        }

        $cmd->db->query("INSERT INTO `work` (`WorkCriterionID`, `WorkCclCriterion`, `WorkDetials`, `WorkScore`, `WorkYear`) Value ('$_POST[WorkCriterionID]', '$_POST[WorkCclCriterion]', '$_POST[WorkDetials]', '$_POST[WorkScore]', '2018')");
        $json_from = [
            'status' => 'successful',
            'file' => $state
        ];
        echo json_encode($json_from);
    }

      // -----------------
      // -- Prepare data Process [GET/POST]
      // -----------------

      //-- query account data
    else if (isset($_GET['showuser'])) {
        $cList = $cmd->db->query("SELECT * FROM `account`")->fetchAll();
        foreach ($cList as $key => $value) {
            $json_result[] = [
                'accid' => $value[0],
                'accname' => $value[1],
                'acclastname' => $value[2],
                'accemail' => $value[3],
                'accphone' => $value[4],
                'accstatus' => $value[5]
            ];
        }
        echo json_encode($json_result);
    }
      
      //-- query criterion data
    else if (isset($_GET['criterions'])) {
        $cList = $cmd->db->query("SELECT `CriterionID`, `CriterionName` FROM `Criterions` ORDER BY CAST(`CriterionID` as SIGNED INTEGER) ASC")->fetchAll();
        foreach ($cList as $key => $value) {
            $json_result[] = [
                'id' => $value[0],
                'name' => $value[1],
            ];
        }
        echo json_encode($json_result);
    }

      //-- query discription of criterion data
    else if (isset($_GET['cdcriterion'])) {
        $crit = 7; // maybe change after
        $i = 0;
        $description = $cmd->db->query("SELECT `CdName` FROM `CriterionDescriptions` WHERE `CdCriterion` = '$crit'")->fetchAll();
        foreach ($description as $key => $value) {
            $json_result[] = [
                'index' => $i++,
                'name' => $value[0]
            ];
        }
        echo json_encode($json_result);
    }

      //-- query scores of criterion data
    else if (isset($_GET['cscriterion'])) {
        $crit = 7; // maybe change after
        $sourcesCri = $cmd->db->query("SELECT `CsName` FROM `CriterionSources` WHERE `CsCriterion` = '$crit'")->fetchAll();
        foreach ($sourcesCri as $key => $value) {
            $json_result[] = [
                'CsName' => $value[0],
            ];
        }
        echo json_encode($json_result);
    }

      //-- query checklist of criterion data
    else if (isset($_GET['cclcriterion'])) {
        $cc = $_GET['cclcriterion'];
        $list = $cmd->db->query("SELECT `CclPoint`, `CclName` FROM `CriterionChecklists` WHERE `CclCriterion` = '$cc' ")->fetchAll();

        foreach ($list as $key => $value) {
            $json_from[] = [
                'no' => $value[0],
                'list' => $value[1]
            ];
        }
        echo json_encode($json_from);
    }

      //-- query viewdata of discription and criterion name
    else if (isset($_GET['point'])) {
        $result = $cmd->db->query("SELECT `CclPoint`, `CclName` FROM `CriterionChecklists` WHERE  `CclCriterion` = '7' AND `CclPoint` = '$_GET[point]'")->fetchAll();
        $criterion = $cmd->db->query("SELECT `CriterionName` FROM `Criterions` WHERE `CriterionID` = '7'")->fetchAll();
        foreach ($result as $key => $value) {
            $json_result[] = [
                'criterion' => $criterion[0][0],
                'no' => $value[0],
                'list' => $value[1]
            ];
        }
        echo json_encode($json_result);

        // -- active when in database have old data
        $tmp_work = $cmd->db->query("SELECT * FROM `Work` WHERE `WorkCriterionID` = '7' AND `WorkCclCriterion` = '$_GET[point]' ORDER BY `WorkID` DESC LIMIT 1")->fetch(PDO::FETCH_ASSOC);
        $tmp_file = $cmd->db->query("SELECT * FROM `File` WHERE `FileCriterionID` = '7' AND `FileCclCriterion` = '$_GET[point]'")->fetchAll();
        foreach ($tmp_file as $key => $value) {

        }
        $data_work[] = [
            'WorkID' => $tmp_data['WorkID'],
            'WorkDetials' => $tmp_data['WorkDetials'],
            'WorkScore' => $tmp_data['WorkScore']
        ];

    } else if (isset($_GET['cclfile'])) { //-- query viewdata of file in ccl(point) 

        $sql = "SELECT f.`FileName`, f.`FileDirectory`, f.`FileCclCriterion`, f.`FileCriterionID`, f.`FileVersion`
        FROM `file` f
          INNER JOIN(
              SELECT `FileName`,`FileCclCriterion`,`FileCriterionID`, MAX(`FileVersion`) AS FileVersion
              FROM `file`
              WHERE `FileCriterionID` = '$_GET[cid]' AND `FileCclCriterion` = '$_GET[ccl]'           
              GROUP BY `FileCriterionID`, `FileCclCriterion`, `FileName`
          ) g
          ON f.`FileVersion` = g.FileVersion
          AND f.`FileName` = g.`FileName`
          AND f.`FileCclCriterion` = g.`FileCclCriterion`
          AND f.`FileCriterionID` = g.`FileCriterionID`;";

        $file = $cmd->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        foreach ($file as $keyline => $line) {
            $tmparr = [];
            foreach ($line as $keydata => $data) {
                $tmparr[$keydata] = $data;
            }
            $json_result[] = $tmparr;
        }
        echo json_encode($json_result);

    } else if (isset($_GET['listfile'])) { //-- query view data of all file for selection  

        $sql = "SELECT f.`FileName`, f.`FileDirectory`, f.`FileCclCriterion`, f.`FileCriterionID`, f.`FileVersion`
        FROM `file` f
          INNER JOIN(
              SELECT `FileName`,`FileCclCriterion`,`FileCriterionID`, MAX(`FileVersion`) AS FileVersion
              FROM `file` 
              GROUP BY `FileCriterionID`, `FileCclCriterion`, `FileName`
          ) g
          ON f.`FileVersion` = g.FileVersion
          AND f.`FileName` = g.`FileName`
          AND f.`FileCclCriterion` = g.`FileCclCriterion`
          AND f.`FileCriterionID` = g.`FileCriterionID`;";

        $file = $cmd->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        foreach ($file as $keyline => $line) {
            $tmparr = [];
            foreach ($line as $keydata => $data) {
                $tmparr[$keydata] = $data;
            }
            $json_result[] = $tmparr;
        }
        echo json_encode($json_result);

    } else if (isset($_GET['workshow'])) { //-- query view detail of checklist in any criterion

        $sql = "SELECT w.`WorkID`, w.`WorkDetials`, w.`WorkScore`, w.`WorkYear`
              FROM `work` w
                INNER JOIN(
                    SELECT `WorkID`, `WorkCriterionID`, `WorkCclCriterion`, MAX(`WorkID`) AS ID
                    FROM `work` 
                    WHERE `WorkCriterionID` = '$_GET[cid]' AND `WorkCclCriterion` = '$_GET[ccl]'
                    GROUP BY `WorkCriterionID`, `WorkCclCriterion`
                ) g
                ON w.`WorkID` = g.ID";

        $rawdata = $cmd->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        foreach ($rawdata as $keyline => $line) {
            $tmparr = [];
            foreach ($line as $keydata => $data) {
                $tmparr[$keydata] = $data;
            }
            $json_result[] = $tmparr;
        }
        echo json_encode($json_result);
    } else {
        echo "Service is running \n you ip is " . $_SERVER['REMOTE_ADDR'];
    }

      /*
      else if(isset($_GET[''])){ //-- Model

      }
     */

    ?>
