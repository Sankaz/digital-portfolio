<?php
 ###################################################
##                Function for PDO               ##
##           Created by Sansern Kanmano          ##
##                 Version 1.03                  ##
###################################################
#         Edit 31012018 subVersion 0.1            #
###################################################
class command
{

    public $db;
    private $HOST_NAME = "sql172.main-hosting.eu";
    private $PORT = "3306";
    private $DB_NAME   = "u966725643_std";       // Database name
    private $CHAR_SET = "charset=utf8";     // Encode type
    private $USERNAME = "u966725643_std";             // Username for login database
    private $PASSWORD = "Stdroot43";                // Password for login database
    public function __construct()
    {
        try {

            $this->db = new PDO('mysql:host=' . $this->HOST_NAME . ';port=' . $this->PORT . ';dbname=' . $this->DB_NAME . ';' . $this->CHAR_SET, $this->USERNAME, $this->PASSWORD);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $this->db;
        } catch (PDOException $e) {
            
            file_put_contents("log/dberror.log", "Date: " . date('M j Y - G:i:s') . " ---- Error: " . $e->getMessage() . PHP_EOL, FILE_APPEND);
            die($e->getMessage());

        }
      

    }

    public function test()
    {
        $this->js_alert("Testing config");
    } // -- For test command

    function checkRegister($username, $idUser)
    {

        // # Function for check Username or IDUser When they register
        // # Enter Table name in DB are you use for collect user data
        // private $attr_TableName = "User";
        // # Filde data's are you want to select
        // private $attr_Selecting = "*";
        // # Variable attribute of field in form
        // # Can add or remove it for adjust your condition
        // private $attr_FmUsername = "";

        $check = $this->db->prepare("SELECT * FROM `User` WHERE username = ? OR idUser = ?");
        $check->execute(array($username, $idUser));
        if ($check->rowCount() >= 1) {
            return true;
        } else {
            return false;
        }
    } // -- end check username and idUser when register --

    function checkLogin($username, $password, $page)
    {
        // SELECT `stdAcc_ID`, `stdAcc_Name`, `stdAcc_Lastname` FROM `stdAccount` WHERE `stdAcc_ID` = '' or `stdAcc_Email` = '' and `stdAcc_ID` = ''
        $check = $this->db->prepare( "SELECT `stdAcc_ID`, `stdAcc_Name`, `stdAcc_Lastname`, `stdAcc_Gender` FROM `stdAccount` WHERE `stdAcc_ID` = ? AND `stdAcc_ID` = ? or `stdAcc_Email` = ? and `stdAcc_ID` = ?");
        $check->execute(array($username, $password, $username, $password));
        if ($check->rowCount() == 1) {
            session_start();
            $data = $check->fetch(PDO::FETCH_ASSOC);
            $_SESSION[IDUser] = $data[stdAcc_ID];
            $_SESSION[statusUser] = 'USER';
            if ($data[stdAcc_Gender] == '1') {
                $gender = "นาย";
            } else {
                $gender = "นางสาว";
            }
            $_SESSION[nameUser] = $gender.$data[stdAcc_Name] . "  " . $data[stdAcc_Lastname];
            $this->js_locat($page);
            session_write_close();
        } else {
            $check = $this->db->prepare( "SELECT `staffAcc_ID`, `staffAcc_Name`, `staffAcc_Lastname` FROM `staffAccount` WHERE `staffAcc_Email` = ? AND `staffAcc_ID` = ?");
            $check->execute(array($username, $password));
            if ($check->rowCount() == 1) {
                session_start();
                $data = $check->fetch(PDO::FETCH_ASSOC);
                $_SESSION[IDUser] = $data[staffAcc_ID];
                $_SESSION[statusUser] = 'ADM';
                $_SESSION[nameUser] = "อ.".$data[staffAcc_Name] . "  " . $data[staffAcc_Lastname];
                $this->js_locat($page);
                session_write_close();
            } else {
                $this->js_alert_error("Something it wrong pls. check Username or Password again");
            }
        }
    } // -- end function for check user login --

    function checkSession($username, $statusUser)
    {
        if (!isset($username) || !isset($statusUser)) {
            $this->js_alert_error("Sorry, You don\'t have permission to access");
            //  $this->js_locat("./index.php");
            exit();
        }
    } // -- check status member login --

    function checkSessionAdmin($username, $statusUser)
    {
        $this->checkSession($username, $statusUser);
        if ($statusUser != "ADM") {
            $this->js_alert_error("Sorry, You don\'t have permission to access");
            //  $this->js_locat("./index.php");
            exit();
        }
    }

    function logout($page)
    {
        session_start();
        session_destroy();
        $this->js_alert("You are now signed out!!");
        $this->js_locat($page);
    } // -- logout for clear session --

    function csqlInsert($nameTable, $rawFormat)
    {
        #   Read me!!
        #   For use with form only (via method POST)

        $format = explode(",", $rawFormat);
        $index = str_replace(" ", "", str_replace("`", "", $rawFormat));
        $index = explode(",", $index);
        $pathFile = "../file/";

        $sqlInsert = 'INSERT INTO `' . $nameTable . '`(';
        foreach ($format as $key => $value) {
            $sqlInsert .= $value;
            if ($key < (count($format) - 1)) {
                $sqlInsert .= ", ";
            }
        }
        $sqlInsert .= ") VALUES(";
        $fileName = "";
        foreach ($index as $key => $value) {
            if ($_POST[$value] == "") {
                foreach ($_FILES[$value] as $fileKey => $fileValue) {
                    if ($fileKey == "name") {
                        $sqlInsert .= "'" . $fileValue . "'";
                        $fileName = $fileValue;
                    } elseif ($fileKey == "tmp_name") {
                        move_uploaded_file($fileValue, $pathFile . $fileName);
                    }
                }
                if ($key < (count($index) - 1)) {
                    $sqlInsert .= ", ";
                }
            } else {
                $sqlInsert .= "'" . $_POST[$value] . "'";
                if ($key < (count($index) - 1)) {
                    $sqlInsert .= ", ";
                }
            }
        }
        $sqlInsert .= ")";

        return $sqlInsert;
    } // -- create insert sql command --

    function renameFile($nameFile, $file)
    {
        $type = pathinfo($file, PATHINFO_EXTENSION);    // check type file
        $name = $nameFile . "." . $type;
        return $name;
        #  Code for move file
        //  $file = $_FILES[filename][name];
        //  $name = $cmd->renameFile($_SESSION[idUser], $file);
        //  move_uploaded_file($_FILES[filename][tmp_name],"doc/".$name);
    } // -- rename files --

    function clearChar($text)
    {
        $antiChar = array(";", "'", "`", "\n", "#", "\"", "(", ")", "[", "]", "{", "}", "@", "$", ",", "!", "~", "^");
        foreach ($antiChar as $key => $value) {
            if ($value == "\n") {
                $text = str_replace($value, "\t", $text);
            } else {
                $text = str_replace($value, "", $text);
            }
        }
        return $text;
    }

    function sql($sql)
    {
        $data = $this->db->query($sql)->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    function diffdatetimes($objstart, $objfinish)
    {
        $dateStart = new DateTime($objstart);
        $dateFinish = new DateTime($objfinish);
        if($dateFinish < $dateStart){
            return false;
        }else{
            return true;
        }
    }

    function __destruct()
    {
        try {
            $this->db = null; //Closes connection
        } catch (PDOException $e) {
            file_put_contents("log/dberror.log", "Date: " . date('M j Y - G:i:s') . " ---- Error: " . $e->getMessage() . PHP_EOL, FILE_APPEND);
            die($e->getMessage());
        }
    }
    



    ##################################################
    ##               Function with JS               ##
    ##################################################

    //-- alert text by js --
    function js_alert($text)
    {
        echo ("<script type='text/javascript'>alert('" . $text . "')</script>");
    }
    //-- alert text error by js --
    function js_alert_error($text)
    {
        echo "<script type='text/javascript'>alert('" . $text . "');window.history.go(-1);</script>";
    }
    //-- refresh page by html --
    function refresh($page)
    {
        echo "<meta http-equiv ='refresh'content='0;URL=" . $page . "'>";
    }
    //-- redirect page by js --
    function js_locat($page)
    {
        echo "<script type='text/javascript'>window.location.href = '$page';</script>";
    }

    ##################################################
    ##                Notes SQL Code                ##
    ##################################################


    // CREATE TABLE `User` (
    // `id` int unsigned zerofill NOT NULL auto_increment,
    // `idUser` varchar(30) NOT NULL,
    // `username` varchar(50) NOT NULL,
    // `password` varchar(50) NOT NULL,
    // `firstName` varchar(120) NOT NULL,
    // `lastName` varchar(120) NOT NULL,
    // `statusUser` varchar(10) NOT NULL default 'STD',
    // `telUser` varchar(120) NULL,
    // `emailUser` varchar(120) NULL,
    // `addrUser` text NULL,
    // PRIMARY KEY (`id`),
    // UNIQUE KEY `id` (`id`)
    // ) ENGINE=INNODB AUTO_INCREMENT=0 ;
} // -- END CLASS --
 