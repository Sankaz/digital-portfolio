<?php

if ($_SESSION[statusUser] != 'ADM') {
    $hidden = "display: none;";
    $std = "";
} else {
    $hidden = "";
    $std = "display: none;";
}


#     
?>

<!-- Navigation -->
<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="home.php"><b>ระบบจัดการกิจกรรมนักศึกษา</b></a>
    </div>
    <!-- /.navbar-header -->

    <ul class="nav navbar-top-links navbar-right">
        <!-- User Menu -->
        <?php echo $_SESSION[nameUser]; ?>
        <li class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-user">
                <!-- <li><a href="profile.php"><i class="fa fa-user fa-fw"></i> User Profile</a>
                </li>
                <li class="divider"></li> -->
                <li><a href="../core/sys/logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
    </ul>
    <!-- /.navbar-top-links -->

    <div class="navbar-default sidebar" role="navigation">
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav" id="side-menu">
                <li>
                    <a href="home.php"><i class="fa fa-dashboard fa-fw"></i> หน้าหลัก</a>
                </li>
                <li style="<?php echo $std ?>">
                    <a href=" profile.php"><i class="fa fa-user fa-fw"></i> ข้อมูลส่วนตัว</a>
                </li>
                <li style="<?php echo $hidden ?>">
                    <a href="events.php"><i class="fa fa-edit fa-fw"></i> กิจกรรม</a>
                </li>
                <li style="<?php echo $hidden ?>">
                    <a href="addevent.php"><i class="fa fa-plus fa-fw"></i> เพิ่มกิจกรรม</a>
                </li>
                <li style="<?php echo $hidden ?>">
                    <a href="students.php"><i class="fa fa-list fa-fw"></i> รายชื่อนักศึกษา</a>
                </li>
            </ul>
        </div>
        <!-- /.sidebar-collapse -->
    </div>
    <!-- /.navbar-static-side -->
</nav>