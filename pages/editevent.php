<!DOCTYPE html>
<html lang="en">

<head>
    <title>Activity digital portfolio system</title>
    <!-- css and script path -->
    <?php include_once 'style.php' ?>
</head>

<body>
    <?php
    $event = $_GET[id];
    $data = $cmd->sql("SELECT event.*, faculty.faculty_Name, staffAccount.staffAcc_Name, staffAccount.staffAcc_Lastname, eventType.eventType_Name
                        FROM event 
                        INNER JOIN faculty on (event.event_Faculty = faculty.faculty_ID) 
                        INNER JOIN staffAccount on (event.event_Staff = staffAccount.staffAcc_ID)
                        INNER JOIN eventType on (event.event_Type = eventType.eventType_ID)
                        WHERE event.event_ID = '$event'");
    $data = $data[0];

    ?>

    <div id="wrapper">

        <!-- menu path -->
        <?php include_once 'navbar.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">แก้ไขกิจกรรม</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <form action="../core/sys/editevent.php?id=<?php echo $_GET[id] ?>" method="post">
                        <div class="panel panel-info">
                            <div class="panel-heading">รายละเอียดกิจกรรม</div>
                            <div class="panel-body">
                                <div class="container col-lg-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: left">ชื่อกิจกรรม</label>
                                            <div class="col-lg-8"><input type="text" class="form-control" name="event_Name" placeholder="ชื่อกิจกรรม" value=<?php echo $data[event_Name] ?> /> </div>
                                        </div>
                                    </div> <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: left">รายละเอียด</label>
                                            <div class="col-lg-8"><textarea class="form-control" name="event_Detail" placeholder="รายละเอียดกิจกรรม" rows="8"><?php echo $data[event_Detail] ?></textarea></div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: left">ประเภทกิจกรรม</label>
                                            <div class="col-lg-4">
                                                <?php $type = $data[event_Type] ?>
                                                <select class="form-control" name="event_Type">
                                                    <option value="1" <?php if ($type == 1) echo "selected" ?>> กิจกรรมบังคับ </option>
                                                    <option value="2" <?php if ($type == 2) echo "selected" ?>> กิจกรรมประชุมเชียร์ </option>
                                                    <option value="3" <?php if ($type == 3) echo "selected" ?>> กิจกรรมบำเพ็ญประโยชน์ </option>
                                                    <option value="4" <?php if ($type == 4) echo "selected" ?>> กิจกรรมอื่นๆ </option>
                                                </select>
                                            </div>
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: left">ชั่วโมงกิจกรรม</label>
                                            <div class="col-lg-2"><input class="form-control" type="text" name="event_Score" value="<?php echo $data[event_Score] ?>"></div>

                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: left">วันที่</label>
                                            <div class="col-lg-3">
                                                <input class="form-control" type="date" name="event_Sdate" value="<?php echo $data[event_Sdate] ?>">
                                            </div>
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: center">ถึง</label>
                                            <div class="col-lg-3">
                                                <input class="form-control" type="date" name="event_Fdate" value="<?php echo $data[event_Fdate] ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: left">เวลา</label>
                                            <div class="col-lg-3">
                                                <input class="form-control" type="time" name="event_Stime" value="<?php echo $data[event_Stime] ?>">
                                            </div>
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: center">ถึง</label>
                                            <div class="col-lg-3">
                                                <input class="form-control" type="time" name="event_Ftime" value="<?php echo $data[event_Ftime] ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: left">ผู้รับผิดชอบ</label>
                                            <label class="col-lg-8 control-label"><?php echo $data[staffAcc_Name] ?> <?php echo $data[staffAcc_Lastname] ?></label>
                                            <input type="hidden" name="event_Staff" value="<?php echo $data[staffAcc_Name] ?> <?php echo $data[staffAcc_Lastname] ?>">
                                        </div>
                                    </div>
                                    <br>
                                </div>
                            </div>
                            <div class="panel-heading">รายละเอียดผู้เข้าร่วม</div>
                            <div class="panel-body">
                                <div class="container col-lg-12">
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: left">นักศึกษาคณะ</label>
                                            <div class="col-lg-8">
                                                <?php $faculty = $data[event_Faculty] ?>
                                                <select class="form-control" name="event_Faculty">
                                                    <option value="20">-- ทุกคณะ --</option>
                                                    <option value="20" <?php if ($faculty == 20) echo "selected" ?>> คณะวิทยาศาสตร์และเทคโนโลยี </option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <label for="eventName" class="col-lg-2 control-label" style="text-align: left">นักศึกษาชั้นปี</label>
                                            <div class="col-lg-10">
                                                <?php $target = $data[event_Target] ?>
                                                <div class="form-check form-check-inline col-lg-3">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="1" name="event_Target" <?php if ($target == 1) echo "checked" ?>>
                                                    <label class="form-check-label" for="inlineCheckbox1"> ชั้นปีที่ 1</label>
                                                </div>
                                                <div class="form-check form-check-inline col-lg-3">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="2" name="event_Target" <?php if ($target == 2) echo "checked" ?>>
                                                    <label class="form-check-label" for="inlineCheckbox1"> ชั้นปีที่ 2</label>
                                                </div>
                                                <div class="form-check form-check-inline col-lg-3">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox3" value="3" name="event_Target" <?php if ($target == 3) echo "checked" ?>>
                                                    <label class="form-check-label" for="inlineCheckbox1"> ชั้นปีที่ 3</label>
                                                </div>
                                                <div class="form-check form-check-inline col-lg-3">
                                                    <input class="form-check-input" type="checkbox" id="inlineCheckbox4" value="4" name="event_Target" <?php if ($target == 4) echo "checked" ?>>
                                                    <label class="form-check-label" for="inlineCheckbox1"> ชั้นปีที่ 4</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-lg-6 col-lg-offset-3">
                                                <div class="col-lg-6"><input class="form-control btn-success" type="submit" value="บันทึกกิจกรรม"></div>
                                                <div class="col-lg-6"><input class="form-control btn-danger" type="reset" value="ล้างกิจกรรม"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-footer">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

</body>

</html>