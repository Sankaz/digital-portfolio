<!DOCTYPE html>
<html lang="en">

<head>
    <title>Activity digital portfolio system</title>
    <!-- css and script path -->
    <?php include_once 'style.php' ?>
</head>

<body>
    <?php

    // SELECT event.event_ID, event.event_Name, faculty.faculty_Name, staffAccount.staffAcc_Name, staffAccount.staffAcc_Lastname FROM event INNER JOIN faculty on (event.event_Faculty = faculty.faculty_ID) INNER JOIN staffAccount on (event.event_Staff = staffAccount.staffAcc_ID)

    ?>


    <div id="wrapper">

        <!-- menu path -->
        <?php include_once 'navbar.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">กิจกรรมทั้งหมด</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="container">
                <!-- /.row -->
                <!-- <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-1 text-left">
                            <a href="addevent.php" class="btn btn-success"><i class="fa fa-plus"> เพิ่มกิจกรรม</i></a>
                        </div>
                    </div>
                </div> -->

                <div class="row">
                    <br>
                    <div class="col-lg-12">
                        <!-- tabel event -->
                        <div class="table-responsive-lg">
                            <table class="table col-lg-12">
                                <thead class="bg-info">
                                    <tr>
                                        <td class="col-lg-1 text-center">#</td>
                                        <td class="col-lg-2 text-center">รหัสนักศึกษา</td>
                                        <td class="col-lg-2 text-center">ชื่อ-นามสกุล</td>
                                        <td class="col-lg-2 text-center">คณะ</td>
                                        <td class="col-lg-2 text-center">สาขาวิชา</td>
                                        <td class="col-lg-2 text-center">E-mail</td>
                                        <td class="col-lg-1 text-center"></td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $std = $cmd->sql("SELECT *, faculty.faculty_Name, major.major_Name  
                                        FROM `stdAccount`
                                        INNER JOIN faculty on (`stdAcc_Faculty` = faculty.faculty_ID)
                                        INNER JOIN major ON (`stdAcc_Major`= major.major_ID)");

                                    foreach ($std as $key => $value) {
                                        if ($value[stdAcc_Gender] == '1') {
                                            $gender = "นาย";
                                        } else {
                                            $gender = "นางสาว";
                                        }
                                        $name = $gender . $value[stdAcc_Name] . " " . $value[stdAcc_Lastname];
                                        ?>
                                        <tr>
                                            <td class="text-center"><?php echo ($key + 1) ?></td>
                                            <td class="text-center"><?php echo $value[stdAcc_ID] ?></td>
                                            <td class="text-left"><?php echo $name ?></td>
                                            <td class="text-center"><?php echo $value[faculty_Name] ?></td>
                                            <td class="text-center"><?php echo $value[major_Name] ?></td>
                                            <td class="text-center"><?php
                                                                    if ($value[stdAcc_Email] == null) {
                                                                        echo "-";
                                                                    } else {
                                                                        echo $value[stdAcc_Email];
                                                                    } ?></td>
                                            <td class="text-center">
                                                <a href="profile.php?id=<?php echo $value[stdAcc_ID] ?>" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- end container -->
            </div>
        </div>
    </div>
    </div>
    <!-- /.row -->
    </div>
    </div>
    <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

</body>

</html>