<!DOCTYPE html>
<html lang="en">

<head>
    <title>Activity digital portfolio system</title>
    <!-- css and script path -->
    <?php include_once 'style.php' ?>
</head>

<body>
    <?php
    $events = $cmd->sql("SELECT event.event_ID, event.event_Name, faculty.faculty_Name, staffAccount.staffAcc_Name, staffAccount.staffAcc_Lastname FROM event INNER JOIN faculty on (event.event_Faculty = faculty.faculty_ID) INNER JOIN staffAccount on (event.event_Staff = staffAccount.staffAcc_ID)");
    // SELECT event.event_ID, event.event_Name, faculty.faculty_Name, staffAccount.staffAcc_Name, staffAccount.staffAcc_Lastname FROM event INNER JOIN faculty on (event.event_Faculty = faculty.faculty_ID) INNER JOIN staffAccount on (event.event_Staff = staffAccount.staffAcc_ID)
    ?>


    <div id="wrapper">

        <!-- menu path -->
        <?php include_once 'navbar.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="page-header">กิจกรรมทั้งหมด</h2>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="container">
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-1 text-left">
                            <a href="addevent.php" class="btn btn-success"><i class="fa fa-plus"> เพิ่มกิจกรรม</i></a>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <br>
                    <!-- tabel event -->
                    <div class="table-responsive-lg">
                        <table class="table col-lg-12">
                            <thead class="bg-info">
                                <tr>
                                    <td class="col-lg-1 text-center">#</td>
                                    <td class="col-lg-3 text-center">ชื่อกิจกรรม</td>
                                    <td class="col-lg-3 text-center">สังกัด</td>
                                    <td class="col-lg-3 text-center">ผู้รับผิดชอบ</td>
                                    <td class="col-lg-2 text-center"></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($events as $key => $value) { ?>
                                    <tr>
                                        <td class="text-center"><?php echo ($key + 1) ?></td>
                                        <td class="text-left"><?php echo $value[event_Name] ?></td>
                                        <td class="text-center"><?php echo $value[faculty_Name] ?></td>
                                        <td class="text-center"><?php echo "อ." . $value[staffAcc_Name] . " " . $value[staffAcc_Lastname] ?></td>
                                        <td class="text-center">

                                            <a href="eventDetail.php?id=<?php echo $value[event_ID] ?>" class="btn btn-info"><i class="fa fa-list"></i></a>
                                            <a href="editevent.php?id=<?php echo $value[event_ID] ?>" class="btn btn-warning"><i class="fa fa-edit"></i></a>
                                            <a href="../core/sys/devent.php?id=<?php echo $value[event_ID] ?>" class="btn btn-danger" onclick="confirm('คุณต้องการลบกิจกรรมนี้ใช่หรือไม่');"><i class="glyphicon glyphicon-trash"></i></a>

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- end container -->
            </div>
        </div>
    </div>
    </div>
    <!-- /.row -->
    </div>
    </div>
    <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

</body>

</html>