<!DOCTYPE html>
<html lang="en">

<head>
    <title>Activity digital portfolio system</title>
    <!-- css and script path -->
    <?php include_once 'style.php' ?>
</head>

<body>
    <?php
    if ($_GET[id] == '' || $_GET[id] == null) {
        $id = $_SESSION[IDUser];
    } else {
        $id = $_GET[id];
    }

    $data = $cmd->sql("SELECT * FROM `stdAccount` WHERE `stdAcc_ID` = '$id'");
    // print_r($data);
    if ($data[0][stdAcc_Gender] == '1') {
        $gender = "นาย";
    } else {
        $gender = "นางสาว";
    }

    $faculty = $cmd->sql("SELECT `faculty_Name` FROM `faculty` WHERE `faculty_ID` = " . $data[0][stdAcc_Faculty]);
    $major = $cmd->sql("SELECT `major_Name` FROM `major` WHERE `major_ID` = " . $data[0][stdAcc_Major]);

    // print_r($faculty);
    ?>

    <div id="wrapper">

        <!-- menu path -->
        <?php include_once 'navbar.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">ข้อมูลนักศึกษา</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row info -->
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="panel panel-info">
                        <div class="panel-heading">ข้อมูลนักศึกษา</div>
                        <div class="panel-body">
                            <p>
                                <div class="col-lg-2">
                                    <picture>
                                        <img src="../source/img/user.png" class="img-fluid img-thumbnail" alt="...">
                                    </picture>
                                </div>
                                <div class="col-lg-10 align-items-center">
                                    <div class="container col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-10">
                                                <b>รหัสนักศึกษา : </b> <?php echo $data[0][stdAcc_ID] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <b>ชื่อ-นามสกุล : </b><?php echo $gender . $data[0][stdAcc_Name] . " " . $data[0][stdAcc_Lastname] ?>
                                            </div>
                                            <div class="col-lg-4">
                                                <b>ชื่อเล่น : </b><?php echo $data[0][stdAcc_Nickname] ?>
                                            </div>
                                            <div class="col-lg-3">
                                                <b>หมู่โลหิต : </b><?php echo $data[0][stdAcc_Blood] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <b>เบอร์โทรศัพท์ : </b><?php echo $data[0][stdAcc_Tel] ?>
                                            </div>
                                            <div class="col-lg-7">
                                                <b>E-mail : </b><?php echo $data[0][stdAcc_Email] ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-5">
                                                <b>คณะ : </b><?php echo $faculty[0][faculty_Name] ?>
                                            </div>
                                            <div class="col-lg-7">
                                                <b>สาขาวิชา : </b><?php echo $major[0][major_Name] ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row info -->
            <?php
            $event1 = $cmd->sql("SELECT joinEvent.join_stdAccID, event.event_ID, SUM(event.event_Score) as total FROM joinEvent INNER JOIN event ON joinEvent.join_EventID = event.event_ID WHERE `join_stdAccID` = '$id' AND event.event_Type= '1'");
            $event1 = $event1[0][total];
            $p1 = ($event1 * 100) / 100;
            $event2 = $cmd->sql("SELECT joinEvent.join_stdAccID, event.event_ID, SUM(event.event_Score) as total FROM joinEvent INNER JOIN event ON joinEvent.join_EventID = event.event_ID WHERE `join_stdAccID` = '$id' AND event.event_Type= '2'");
            $event2 = $event2[0][total];
            $p2 = ($event2 * 100) / 60;
            $event3 = $cmd->sql("SELECT joinEvent.join_stdAccID, event.event_ID, SUM(event.event_Score) as total FROM joinEvent INNER JOIN event ON joinEvent.join_EventID = event.event_ID WHERE `join_stdAccID` = '$id' AND event.event_Type= '3'");
            $event3 = $event3[0][total];
            $p3 = ($event3 * 100) / 60;
            $event4 = $cmd->sql("SELECT joinEvent.join_stdAccID, event.event_ID, SUM(event.event_Score) as total FROM joinEvent INNER JOIN event ON joinEvent.join_EventID = event.event_ID WHERE `join_stdAccID` = '$id' AND event.event_Type= '4'");
            $event4 = $event4[0][total];
            $p4 = ($event4 * 100) / 40;

            ?>
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            สถานะการเข้าร่วมกิจกรรม
                        </div>
                        <div class="panel-body">
                            <div class="container col-lg-12">
                                <div class="row">
                                    <div class="col-lg-3 center"><b>ประเภทกิจกรรม</b></div>
                                    <div class="col-lg-5 center"><b>สถานะ</b></div>
                                    <div class="col-lg-2 center"><b>จำนวน</b></div>
                                    <div class="col-lg-2 center"><b>ผลการประเมิน</b></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-3 center">กิจกรรมบังคับ</div>
                                    <div class="col-lg-5">
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo $p1 ?>%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"><?php echo ceil($p1) ?>%</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 center"><?php if ($event1 == null) {
                                                                        echo "0";
                                                                    } else {
                                                                        echo $event1;
                                                                    }; ?> / 100 ชั่วโมง</div>
                                    <div class="col-lg-2 center"><button type="button" class="btn <?php if ($p1 == 100) echo "btn-success"; else echo "btn-danger"; ?> btn-circle"><i class="<?php if ($p1 == 100) echo "fa fa-check"; else echo "fa fa-times"; ?>"></i></button></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-3 center">กิจกรรมประชุมเชียร์</div>
                                    <div class="col-lg-5">
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo $p2 ?>%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="60"><?php echo ceil($p2) ?>%</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 center"><?php if ($event2 == null) {
                                                                        echo "0";
                                                                    } else {
                                                                        echo $event2;
                                                                    }; ?> / 60 ชั่วโมง</div>
                                    <div class="col-lg-2 center"><button type="button" class="btn <?php if ($p1 == 100) echo "btn-success"; else echo "btn-danger"; ?> btn-circle"><i class="<?php if ($p2 == 100) echo "fa fa-check"; else echo "fa fa-times"; ?>"></i></button></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-3 center">กิจกรรมบำเพ็ญประโยชน์</div>
                                    <div class="col-lg-5">
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo $p3 ?>%;" aria-valuenow="30" aria-valuemin="0" aria-valuemax="60"><?php echo ceil($p3) ?>%</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 center"><?php if ($event3 == null) {
                                                                        echo "0";
                                                                    } else {
                                                                        echo $event3;
                                                                    }; ?> / 60 ชั่วโมง</div>
                                    <div class="col-lg-2 center"><button type="button" class="btn <?php if ($p1 == 100) echo "btn-success"; else echo "btn-danger"; ?> btn-circle"><i class="<?php if ($p3 == 100) echo "fa fa-check"; else echo "fa fa-times"; ?>"></i></button></div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-lg-3 center">กิจกรรมอื่นๆ</div>
                                    <div class="col-lg-5">
                                        <div class="progress">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: <?php echo $p4 ?>%" aria-valuenow="30" aria-valuemin="0" aria-valuemax="40"><?php echo ceil($p4) ?>%</div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2 center"><?php if ($event4 == null) {
                                                                        echo "0";
                                                                    } else {
                                                                        echo $event4;
                                                                    }; ?> / 40 ชั่วโมง</div>
                                    <div class="col-lg-2 center"><button type="button" class="btn <?php if ($p1 == 100) echo "btn-success"; else echo "btn-danger"; ?> btn-circle"><i class="<?php if ($p4 == 100) echo "fa fa-check"; else echo "fa fa-times"; ?>"></i></button></div>
                                </div>
                            </div>
                        </div>
                        <!-- end panel body :: status -->
                    </div>
                    <!-- end panel info -->
                </div>
                <!-- end col-12 -->
            </div>
            <!-- end row status -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

</body>

</html>