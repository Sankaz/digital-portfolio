<!DOCTYPE html>
<html lang="en">

<head>
    <title>Activity digital portfolio system</title>
    <!-- css and script path -->
    <?php include_once 'style.php' ?>
</head>

<body>

    <div id="wrapper">

        <!-- menu path -->
        <?php include_once 'navbar.php' ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">ข่าวกิจกรรม</h3>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row info -->
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1">
                    <div class="panel panel-info">
                        <div class="panel-heading">ข่าวกิจกรรม</div>
                        <div class="panel-body">
                            <p>
                                <div class="text-center">
                                    <img src="../source/img/news1.jpg" class="rounded" alt="" style="width:80%">
                                </div>
                            </p>

                            <p>
                                <div class="text-center">
                                    <img src="../source/img/news2.jpg" class="rounded" alt="" style="width:80%">
                                </div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row info -->
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

</body>

</html> 